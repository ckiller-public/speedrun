require "minitest/autorun"
require "speedrun"

class SpeedrunTest < Minitest::Test
  def test_english_hello
    assert_equal "hello world",
    Speedrun.hi("english")
  end

  def test_any_hello
    assert_equal "hello world",
    Speedrun.hi("ruby")
  end

  def test_spanish_hello
    assert_equal "hola mundo",
    Speedrun.hi("spanish")
  end
end