require File.expand_path("../lib/speedrun/version", __FILE__)

Gem::Specification.new do |s|
  s.name        = "speedrun"
  s.version     = NewGem::VERSION
  s.platform    = Gem::Platform::RUBY
  s.authors     = ["JB Renard"]
  s.email       = ["reanrdjb@gmail.com"]
  s.homepage    = "https://framagit.org/ckiller-public/speedrun"
  s.summary     = "Generate doc from repository"
  s.description = "Usefull generated doc from asciidoc"
  s.license     = "MIT"

  s.required_rubygems_version = ">= 3.1.3"

  # If you have other dependencies, add them here
  # s.add_dependency "another", "~> 1.2"
  s.add_runtime_dependency  "asciidoctor", "~> 2.0"
  s.add_runtime_dependency  "asciidoctor-diagram", "~> 2.2"
  s.add_runtime_dependency  "asciidoctor-diagram-plantuml", "~> 1.2022"
  s.add_runtime_dependency  "asciidoctor-pdf", '~> 2.3'

  
  # If you need to check in files that aren't .rb files, add them here
  s.files        = Dir["{lib}/**/*.rb", "bin/*", "LICENSE", "*.md"]
  s.require_path = 'lib'

  # If you need an executable, add it here
  s.executables = ["speedrun"]

  # If you have C extensions, uncomment this line
  # s.extensions = "ext/extconf.rb"
end